<?php

namespace Tests\Feature;

use Tests\ControllerTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use App\Jobs\CreateCourses;
use App\Course;
use Maatwebsite\Excel\Facades\Excel;

class CourseTest extends ControllerTestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $courses = factory(Course::class, 6)->create();

        $this->courses = $courses;
    }

    /** @test */
    public function it_batch_creates_courses()
    {
        Bus::fake();

        $course = factory(Course::class)->make();
        $response = $this->post('api/courses/add_courses', [
            'text' => $course->text,
        ]);

        Bus::assertDispatched(CreateCourses::class, function ($job) use ($course) {
            return $job->template['text'] === $course['text'];
        });

        $response->assertOk();

        $response->assertExactJson(['success' => 'Job queued']);
    }

    /** @test */
    public function it_registers_a_users_courses()
    {
        $this->user->courses()->sync([]);
        $selected_courses = $this->courses->take(rand(-4, 5))->modelKeys();
        $response = $this->post('api/courses/register', [
            'courses' => $selected_courses,
        ]);
        $response->assertOk();

        $response->assertExactJson([
            'success' => 'Courses registered',
            'detail' => [
                "attached" => array_values($selected_courses),
                "detached" => [],
                "updated" => [],
            ]
        ]);
    }

    /** @test */
    public function it_lists_courses_with_users_registration_status()
    {
        $selected_courses = $this->courses->take(rand(-4, 5))->modelKeys();
        $date = date('Y-m-d\TH:i:s.vv\Z');

        $this->user->courses()->sync($selected_courses);
        $response = $this->get('api/courses/list');
        $response->assertOk();
        $expected = $this->courses->map(
            function ($course) use ($selected_courses, $date) {
                if (collect($selected_courses)->contains($course->id)) {
                    $course['date_enrolled'] = $date;
                }
                return $course;
            }
        )->toArray();
        $response->assertJson($expected);
    }

    /**
     * @test
     */
    public function user_can_download_courses_export_xlsx()
    {
        Excel::fake();

        $this->get('api/courses/export');

        Excel::assertDownloaded('courses.xlsx', function () {
            return true;
        });
    }

    /**
     * @test
     */
    public function user_can_download_courses_export_csv()
    {
        Excel::fake();

        $this->get('api/courses/export?format=csv');

        Excel::assertDownloaded('courses.csv', function () {
            return true;
        });
    }
}
