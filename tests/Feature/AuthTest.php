<?php

namespace Tests\Feature;

use Tests\TestCase;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class AuthTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function it_will_register_a_user()
    {
        $password = $this->faker->password(8);
        $user = factory(User::class)->make(['password' => $password]);

        $response = $this->post('api/auth/register', [
            'name' => $user->name,
            'email'    => $user->email,
            'password' => $password,
            'password_confirmation' => $password,
        ]);
        
        $response->assertStatus(201);

        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in'
        ]);
    }

    /** @test */
    public function it_will_log_a_user_in()
    {
        $password = $this->faker->password(8);
        $user = factory(User::class)->create(['password' => $password]);

        $response = $this->post('api/auth/login', [
            'email'    => $user->email,
            'password' => $password,
        ]);

        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in'
        ]);
    }

    /** @test */
    public function it_will_not_log_an_invalid_user_in()
    {
        $password = $this->faker->password(8);
        $user = factory(User::class)->make(['password' => $password]);

        $response = $this->post('api/auth/login', [
            'email'    => $user->email,
            'password' => $password,
        ]);

        $response->assertJsonStructure([
            'error',
        ]);
    }
}
