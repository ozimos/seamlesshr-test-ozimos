<?php

namespace Tests;

use Tests\TestCase;
use App\User;

abstract class ControllerTestCase extends TestCase
{
    protected $user;

    /**
     * Setup the DB before each test.
     */
    public function setUp(): void
    {
        parent::setUp();
        $user = User::first();
        if (! $user) {
            $user = factory(User::class)->create(['name' => 'Test User']);
        }

        $this->actingAs($user, 'api');
        $this->user = $user;
    }
}
