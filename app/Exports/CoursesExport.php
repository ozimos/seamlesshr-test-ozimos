<?php

namespace App\Exports;

use App\Course;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CoursesExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Course::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'Text',
            'Date Created',
            'Date Modified',
            'Title',
            'Code',
            'Credit Units'
        ];
    }
}
