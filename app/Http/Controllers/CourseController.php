<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Jobs\CreateCourses;
use Illuminate\Support\Facades\Auth;
use App\Exports\CoursesExport;
use Maatwebsite\Excel\Facades\Excel;

class CourseController extends Controller
{
    public function add_courses(Request $request)
    {
        $request->validate([
            'courses' => ['sometimes', 'required', 'array', 'max:100'],
            'courses.*' => ['integer']
        ]);
        $template = $request->only(['text', 'title', 'code', 'credit']);
        CreateCourses::dispatch($template);

        return response()->json(['success' => 'Job queued']);
    }

    public function register(Request $request)
    {
        $request->validate([
            'text' => ['sometimes', 'string', 'max:255'],
            'title' => ['sometimes', 'string', 'max:50'],
            'code' => ['sometimes', 'alpha_num', 'max:10'],
            'credit' => ['sometimes', 'integer', 'max:10']
        ]);
        $result = $request->user()->courses()->syncWithoutDetaching($request->courses);

        return response()->json([
            'success' => 'Courses registered',
            'detail' => $result
        ]);
    }

    public function list(Request $request)
    {
        $courses = Course::with('users')->get();
        $courses->each(function ($course) {
            $user = $course->users->firstWhere('id', Auth::id());
            unset($course->users);
            if ($user) {
                $course['date_enrolled'] = $user->registeredUsers->created_at;
            }
        });
        return $courses;
    }

    public function export(Request $request) 
    {
        $format = $request->query('format') === 'csv' ? 'csv' : 'xlsx';
        return Excel::download(new CoursesExport, 'courses.' . $format);
    }
}
