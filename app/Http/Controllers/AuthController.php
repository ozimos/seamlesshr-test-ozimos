<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\User;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => ['sometimes', 'required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed' ],
        ]);

        $user = User::create([
            'email'    => $request->email,
            'name'    => $request->name,
            'password' => $request->password,
        ]);

        $token = auth()->login($user);

        return $this->respondWithToken($token, 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8' ],
        ]);

        $credentials = $request->only(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        try {
            if ($token = auth()->refresh()) {
                $response = ['meta' => ['token' => $token]];
            }
        } catch (Exception $error) {
            $response = response()->json(['error' => 'refresh_token_error'], 422);
        }

        return $response;
    }

    protected function respondWithToken($token, $statusCode = 200)
    {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60
        ], $statusCode);
    }
}
