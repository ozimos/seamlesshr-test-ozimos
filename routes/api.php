<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')
    ->get('/user', function (Request $request) {
        return $request->user();
    })->name('user');

Route::prefix('auth')->name('auth.')->group(function () {
    Route::post('register', 'AuthController@register')->name('register');
    Route::post('login', 'AuthController@login')->name('login');
    Route::get('refresh', 'AuthController@refresh')->name('refresh');
    Route::get('logout', 'AuthController@logout')
        ->middleware('auth:api')->name('logout');
});

Route::prefix('courses')->name('courses.')->middleware('auth:api')
    ->group(function () {
        Route::post('add_courses', 'CourseController@add_courses')
            ->name('add_courses');
        Route::post('register', 'CourseController@register')->name('register');
        Route::get('list', 'CourseController@list')->name('list');
        Route::get('export', 'CourseController@export')->name('export');
    });
