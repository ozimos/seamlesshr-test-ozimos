<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'text' => $faker->sentence,
        'title' => $faker->words(rand(3, 6), true),
        'code' => 'STA101',
        'credit' => rand(2, 6),
    ];
});
